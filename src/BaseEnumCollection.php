<?php

declare(strict_types = 1);

namespace Gupo\Enum;

use Gupo\Enum\Contracts\EnumCollection;

/**
 * 枚举集合基类
 */
abstract class BaseEnumCollection extends BaseEnum implements EnumCollection
{
}
